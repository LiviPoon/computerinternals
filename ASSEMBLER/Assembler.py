import sys

ERROR = 'ff'

#setting maxes and mins of the assembler
registerMin = 0
registerMax = 7
minSigned = -128
maxSigned = 127
minUnsigned = 0
maxUnsigned = 255

#setting all of the commands and error cases
TEST_CASES = {
    "_add 2 1 _regWRI 1 _regWRI 2" : "00210b10b2",
    "_add 2 1 _regWRI 1 _regWRI 2" : "021b1b2",
    "_and 2 1 _regWRI 2 _regWRI 4" : "121b2b4",
    "_or 2 1 _regWRI 3 _regWRI 5" :  "221b3b5" ,
    "_eq 2 1 _regWRI 4 _regWRI 6" :  "521b4b6",
    "_or 2 1 _regWRI 1 _regWRI 1" : "221b1b1",
    "_and 2 1 _regWRI 2 _regWRI 1" :  "121b2b1",
    "_lt 2 1 _regWRI 3 _regWRI 2" : "721b3b2",
    "_mt 2 1 _regWRI 1 _regWRI 2" : "621b1b2",
    "_xor 2 1" : "321",
    "_regRAD 1 _regWRI 2" : "c1b2",
    "_lteq 00000100 010100101 _ramCON 00000001" : "84a513",
    "_or 00010100 00010100 _regWRI 00000001 _regWRI 00000010 _regRAD 00000100" : "21414b1b2c4",
    "_sub 00010100 _inv 00010100 _regWRI 00000001 _ramSTR 00000010" : "414f14b1102",
    "_add 00000111 00000110 _regWRI 00000110 _regWRI 00010010" : "076b6bFF ERROR",
    "_add 00000111 00000110 _regWRI 00001110 _regWRI 00010011" : "076bFFbFF ERROR",
    "_add 00000111 00000110 _regWRI 01000110 _regWRI 00010010" : "076bFFbFF ERROR",



}

AUL_COMMANDS = {
    '_add': '0',
    '_and': '1',
    '_or':  '2',
    '_xor': '3',
    '_sub': '4',
    '_eq':  '5',
    '_mt':  '6',
    '_lt':  '7',
    '_lteq':  '8',
    '_mteq':  '9',
    '_inJump':'a',
    '_inv':   'f',
    '_conJump':'4',
}

REG_COMMANDS = {
     '_regWRI':'b',
     '_regRAD':'c',
     '_regLWD':'d',
     '_regCLR':'e',
}

RAM_COMMANDS = {
    '_ramSTR':'10',
    '_ramLWD':'11',
    '_ramRAD':'12',
    '_ramCON':'13',

 }

#Take assembly code and dicing it up into lines

def display_error(msg, line_num):
    sys.stderr.write('Error on line {}: {}\n'.format(line_num,  msg))

def runAssember():
    outputHex = []
    error = False
    inputedAssemblyCode = open("assemblyCode.txt","r")
    outputMachineCode = open("machineCode.txt","w")
    assemblyCodeLines = inputedAssemblyCode.readlines()

    outputMachineCode.write("v2.0 raw\n")

    for j in range(len(assemblyCodeLines)): #Run for every line in the assembly code
        splitAssemblyCode = assemblyCodeLines[j].split()
        outputMachineCode.write("\n")


        for i in range(len(splitAssemblyCode)):
            assemblyCodeCharacters = set(splitAssemblyCode[i]) #check if the number is binary
            binaryValues = {'0', '1'}

            previousCommand = int(i-1)#check if the previous command was a register command, and make sure its not an invald register
            if previousCommand == -1: #prevent syntax error
                previousCommand = 0

            if str(splitAssemblyCode[i]) in AUL_COMMANDS.keys():#check if its in the AUL COMMANDS
                outputMachineCode.write(AUL_COMMANDS.get(str(splitAssemblyCode[i])))


            elif str(splitAssemblyCode[i]) in REG_COMMANDS.keys():
                outputMachineCode.write(REG_COMMANDS.get(str(splitAssemblyCode[i])))


            elif str(splitAssemblyCode[i]) in RAM_COMMANDS.keys():
                outputMachineCode.write(RAM_COMMANDS.get(str(splitAssemblyCode[i])))

            #check if the previous command was a register command, and check to see if the current command is an invalid ADDRESS
            elif splitAssemblyCode[previousCommand] in REG_COMMANDS.keys():

                if int(splitAssemblyCode[i]) < 0:
                    display_error("INVALID REGISTER ADDRESS (MIN): ", str(j))
                    outputMachineCode.write("FF")
                    print(splitAssemblyCode[i])
                    error = True

                elif int(splitAssemblyCode[i]) > 7:
                    display_error("INVALID REGISTER ADDRESS (MAX): ", str(j))
                    outputMachineCode.write("FF")
                    print(splitAssemblyCode[i])
                    error = True

                else:
                    outputMachineCode.write(hex(int(splitAssemblyCode[i]))[2:4]) #if it doesn't fail just write the output

            #check if the previous command was a ram command, and make sure its not an invald ram
            elif splitAssemblyCode[previousCommand] in RAM_COMMANDS.keys():
                if int(splitAssemblyCode[i]) < minSigned:
                    display_error("INVALID RAM ADDRESS: (MIN)", str(j))
                    outputMachineCode.write("F F")
                    print(splitAssemblyCode[i])
                    error = True

                elif int(splitAssemblyCode[i]) > maxSigned:
                    display_error("INVALID RAM ADDRESS: (MAX)", str(j))
                    outputMachineCode.write("FF")
                    print(splitAssemblyCode[i])
                    error = True

                elif splitAssemblyCode[previousCommand] == '_ramCON': #check if the constant is either on or off
                    if splitAssemblyCode[i] == 0 or splitAssemblyCode[i] == 1:
                        pass
                    else:
                        display_error("INVALID CONSTANT: ", str(j))
                        outputMachineCode.write("FF")
                        print(splitAssemblyCode[i])
                        error = True
                else:
                    outputMachineCode.write(hex(int(splitAssemblyCode[i]))[2:4]) #if it doesn't fail just write the output


            #if the value does not pass any of the past valitity checks, check if its an integer
            elif int(splitAssemblyCode[i]) & 1 == 0 or int(splitAssemblyCode[i]) == 1:
                if int(splitAssemblyCode[i]) < minUnsigned or int(splitAssemblyCode[i]) > maxUnsigned:
                    display_error("INVALID VALUE: ", str(j))
                    outputMachineCode.write("FF")
                    print(splitAssemblyCode[i])
                    error = True

                else:
                    outputMachineCode.write(hex(int(splitAssemblyCode[i]))[2:4])

            else: #if its not binary, then stop the loop
                print(splitAssemblyCode[i])
                display_error("INVALID COMMAND: ", str(j))
                outputMachineCode.write("FF")
                print(splitAssemblyCode[i])
                error = True

        if error == True:
            outputMachineCode.write(" ERROR")
            error = False

        elif str(assemblyCodeLines[j]) in TEST_CASES.keys() and i <= len(TESTCASES):
            outputMachineCode.close()
            outputMachineCode = open("machineCode.txt","r")
            machineCodeLines = outputMachineCode.readlines()
            if str(assemblyCodeLines[j]) in TESTCASES.keys() and str(machineCodeLines[j]) in TESTCASES.values():
                print("Test Case: TRUE")

            if TESTCASES[str(assemblyCodeLines[j])] != machineCodeLines[j]:
                print("Test Case: FALSE")

            else:
                pass



runAssember()
