This project is a bunch of my work from my class Computer Internals, which I took
at The Nueva School.

ALL FOLLOWING CONTENT, WITHIN THIS README FILE, IS OWNED/WRITTEN BY THE COURSE DIRECTOR (the teacher) 
JEN SELBY in CORRELATION with the NUEVA SCHOOL.


*COURSE: Computer Internals - Jen Selby*

SYLLABUS:

This course builds students’ competence in mathematical reasoning, focusing on 
generalizing patterns, building strong arguments, and finding multiple approaches
to solving problems. Students will learn to ask probing questions, reflect on 
their problem-solving process, and clearly communicate their findings. Students 
will develop mathematical fluency by integrating geometry and algebra with rich 
introductions to sets, logic and abstract algebra, quadratic and transcendental 
functions, congruence and similarity of triangles, formal proof and notation, 
descriptive statistics and vectors, . The underlying focus for the year is on 
building the language and foundations of mathematics.

GOALS:

1. Generalize trends from patterns so as to ask questions, form hypotheses, test
special cases, and prove observed trends

2. Cultivate the metacognition to self-identify one’s understanding/processing 
in order to recognize misunderstanding

3. Construct sound proofs; evaluate the soundness of proofs; identify logical 
flaws in proofs and address them

4. Instinctively bring a mathematical framework to problems; demonstrate the 
skills to abstract problems into the mathematical space and the skills to apply 
the result back to the real world

5. Use functions as a formal means of abstracting and capturing real-world 
phenomena in mathematical terms

6. Work from an understanding that many problems have multiple approaches so as 
to motivate finding creative and elegant solutions to problems and looking for 
connections

7. Use the design thinking process to break complex problems into smaller pieces
and pursue promising leads and test cases, iterating to uncover underlying patterns

8. Use technology to explore, extrapolate, and calculate with greater precision

9. Clearly state mathematical arguments and communicate mathematical results, 
identifying the core ideas of an argument and analyzing others’ thinking and 
argumentation

10. Be able to work productively both independently and with others; collaborate 
effectively, communicating one’s thoughts and learning from and supporting 
others’ efforts

11. Make personal connections to mathematics, develop curiosity and self-agency 
as users and creators of mathematics, develop appreciation for beauty and 
elegance in mathematical thinking, and seek out challenges and persevere through 
them


CONTENT OBJECTIVES (LEARNING OUTCOMES) 

Unit 1: Sets and Logic

Convert between verbal descriptions and set and formal logic notation using set 
and logic symbols and Venn diagrams

Define set operations (union, intersection, complement, set-difference) and 
logical connectives (conjunction, disjunction, conditional, negation)

Identify and analyze finite/infinite/ null or universal sets as well as 
subsets, power sets and cardinality

Define and apply set builder notation to write sets or describe the general 
form of a given set.

Combine and verify the truth of logical statements (including, but not limited 
to: conditional, negation, contrapositive, truth tables)

Recognize and apply inductive and deductive reasoning (including, but not 
limited to: constructing statements)

Unit 2: Geometry - Angles, Similarity, Congruence, Triangles

    Perform and explain basic constructions
    
    Identify and apply congruent angles in proofs/problems
    
    Identify and apply congruent triangles in proofs/problems
    
    Identify and apply similar triangles in proofs/problems (including, 
    but not limited to: scale factors, areas)

Unit 3: Functions, transformations (Fall and Spring)

    Identify and translate between different representations of functions 
    (problem space, table, graph, algebraic) (including, but not limited to: 
    use function notation, find domain and range, inverses, compositions)
    
    Identify and connect representations of a function, especially for linear, 
    quadratic, exponential, absolute value, and piecewise functions (including, 
    but not limited to: use function notation, find domain and range, inverses, 
    compositions)
    
    Transform functions via horizontal and vertical translations, reflections, 
    and stretches
    
    Identify key features of functions, including intercepts; intervals where 
    the function is increasing, decreasing, positive, or negative; relative 
    maximums and minimums; symmetries; even or oddness; end behavior; and 
    periodicity

Unit 4: Systems of equations

    Solve equations (including, but not limited to: linear, quadratic, radical, 
    rational)
    
    Convert between different forms of functions (including, but not limited to:
    vertex form)
    
    Solve and apply general systems of equations to problems (including, but not
    limited to 2D, 3D, non-linear)
    
    Solve optimization problems using systems of equations and inequalities 
    (including, but not limited to linear programming)

Unit 5: Descriptive Stats

    Represent data using appropriate graphs (histograms, box plots, normal 
    probability plot)
    
    Evaluate and describe measures of center (mean, median, mode) and spread 
    (range, variance, standard deviation) and identify outliers using IQR
    
    Evaluate and describe measures of position (z-scores), test for outliers 
    (normal curve), and determine probability using normal distribution
    
    Use technology to find and analyze least squares regression models to 
    understand goodness of fit (r2) and correlation (r)

Unit 6: Right Triangle and Unit Circle Trigonometry

    Calculate and apply angles and lengths of right triangles using 
    trigonometric ratios
    
    Recognize and apply special triangles (30-60-90, 45-45-90)
    
    Convert between coordinates and angles (in degrees and radians) on the 
    unit circle
    
    Explore symmetry and patterns to connect trigonometric functions on the 
    unit circle, including an introduction to identities and transformations

Unit 7: Trigonometric Functions (sine, cosine, inverse trigonometry)

    Apply concepts of transformations to graph cosine and sine functions 
    (in degrees and radians)(amplitude, phase shift, period and vertical
    translation)
    
    Apply cosine and sine functions to model real world periodic phenomena
    
    Apply cosine and sine functions to solve problems (including, but not 
    limited to kinematics and dynamics)
    
    Explore solutions to trigonometric equations using the unit circle using 
    inverse sine, inverse cosine and inverse tangent (in degrees and radians)